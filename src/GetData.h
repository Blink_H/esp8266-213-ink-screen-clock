#ifndef _GetData_h
#define _GetData_h

#include "DefineData.h"
//NTP时间服务器函数
#include <WiFiUdp.h>
#include <TimeLib.h>
#include <NTPClient.h>

#include "JsonWeather.h"
#include "CallHttps.h"
#include "display_demo.h"

#include <Adafruit_Sensor.h>
#include "DHT.h"

void NTP_init();
void DHT11_init();
void Get_Time();
void Get_TianQi();
void Get_Dht11data();
#endif