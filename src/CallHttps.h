#ifndef _CallHttps_h
#define _CallHttps_h

#include <Arduino.h>
#include <ESP8266HTTPClient.h>

String callHttps(String url);  //https请求，如果可用堆太小，会直接失败
String callHttp(String url);  //http请求

#endif