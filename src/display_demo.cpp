#include "display_demo.h"



//带温湿度的位置
uint16_t day_x0 = 54;        //未来天气详情X坐标

#define day_y0 90  //未来天气详情Y坐标
#define day_y1 day_y0+18
#define day_y2 day_y1+18
//小提示图标位置
#define main_y0 14         //更新时间图标
#define main_y1 main_y0+19   //位置图标
#define main_y2 main_y1+19   //天气状态图标

#define main_x0 1         // 左边小图标X位置
#define main_x1 main_x0+15  // 左边小图标后的文字的X位置

#define main_x2 295-12    // 右边小图标X位置
//实况温度位置
uint16_t temp_x;
#define temp_y0 50
//圈圈位置
uint16_t circle_x;



//定义DefineData里的全局变量
Define_Data Data;

SPISettings spi_settings(4000000, MSBFIRST, SPI_MODE0);

//声明外部字体常量
extern const uint8_t chinese_city_gb2312[239032] U8G2_FONT_SECTION("chinese_city_gb2312");
// GxEPD2_BW<GxEPD2_290_T94, GxEPD2_290_T94::HEIGHT> display(GxEPD2_290_T94(/*CS=D8*/ SS, /*DC=D3*/ 0, /*RST=D4*/ 2, /*BUSY=D2*/ 4)); // GDEM029T94
GxEPD2_BW<GxEPD2_213, GxEPD2_213::HEIGHT> display(GxEPD2_213(/*CS=*/ 16, /*DC=D3*/ 0, /*RST=*/ -1, /*BUSY=*/ 4));
U8G2_FOR_ADAFRUIT_GFX u8g2Fonts;

void display_init()   // 2.13寸屏幕初始化
{
  pinMode(RST_PIN, OUTPUT);
  digitalWrite(RST_PIN, HIGH);
  delay(20);
  digitalWrite(RST_PIN, LOW);
  delay(20);
  digitalWrite(RST_PIN, HIGH);
  delay(200);

  display.init();
  display.setRotation(3);        // 设置方向  设置横屏  
  // display.setRotation(2);        // 设置方向  设置横屏  

  u8g2Fonts.begin(display);                        // 将u8g2过程连接到Adafruit GFX
  u8g2Fonts.setFontMode(1);                        // 使用u8g2透明模式（这是默认设置）
  u8g2Fonts.setFontDirection(0);                   // 从左到右（这是默认设置）
  u8g2Fonts.setForegroundColor(GxEPD_BLACK);             // 设置前景色
  u8g2Fonts.setBackgroundColor(GxEPD_WHITE);             // 设置背景色
  u8g2Fonts.setFont(chinese_city_gb2312);
  
}
void BW_refresh() //黑白刷新一次
{
  display.init();
  display.fillScreen(GxEPD_BLACK);  // 填充屏幕
  display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲
  display.fillScreen(GxEPD_WHITE);
  display.display(1);
}

void display_ErWeiMa()    //显示二维码
{
  
  display.setPartialWindow(0, 0, 120, 150);//局刷提示字样
  u8g2Fonts.setFont(chinese_city_gb2312);
  display.firstPage();

  // BW_refresh(); //黑白刷新一次

  display.drawInvertedBitmap(5, 0, gImage_Peiwang, 80, 80, GxEPD_BLACK); //二维码图标

  u8g2Fonts.setCursor(5,95);
  u8g2Fonts.print("打开微信扫码配网");
  display.nextPage();
}


//**************************************************************************************************
void smartConfig()   //智能配网
{
    // 显示配网二维码
    display_ErWeiMa();
    int i = 0;

    WiFi.mode(WIFI_STA);
    Serial.println("\r\nWait for Smartconfig");
    WiFi.beginSmartConfig();
    for (i = 0; i < 30; i++)
    {
       Serial.print(".");
       if(WiFi.smartConfigDone())
       {
          Serial.println("SmartConfig Success");
          Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
          Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());
          WiFi.setAutoConnect(true);  // 设置自动连接
          
          Serial.println("WiFi connected");
          Serial.println(WiFi.localIP());
          Serial.println("Starting UDP");

          // Udp.begin(localPort);
          // Serial.print("Local port: ");
          // Serial.println(Udp.localPort());

          u8g2Fonts.setCursor(85,5);
          u8g2Fonts.print("WIFI连接成功!");
          display.nextPage();

          delay(1000); 
          ESP.restart();
          break;
       }
      u8g2Fonts.setCursor(5,110);
      u8g2Fonts.print("连接中...");
      display.nextPage();
      delay(1000);
    }
    if (i > 28)
    {
       delay(1000);
       Serial.println("SmartConfig Faild!" );
       u8g2Fonts.setCursor(85,15);
       u8g2Fonts.print("WIFI连接失败!");
       display.nextPage();
    }
}

bool autoConfig()   //自动连接WiFi
{
    WiFi.begin();                    // 默认连接保存的WIFI

    for (int i = 0; i < 10; i++)
    {      
       if (WiFi.status() == WL_CONNECTED)
       {
          display.firstPage();
          u8g2Fonts.setCursor(5,95);
          u8g2Fonts.print("WIFI连接成功!");
          display.nextPage();
          delay(500);
          BW_refresh(); //黑白刷新一次

          Serial.println("AutoConfig Success");
          Serial.printf("SSID:%s\r\n", WiFi.SSID().c_str());
          Serial.printf("PSW:%s\r\n", WiFi.psk().c_str());

          Serial.println("WiFi connected");
          Serial.println(WiFi.localIP());
          // Serial.println("Starting UDP");
          // Udp.begin(localPort);
          // Serial.print("Local port: ");
          // Serial.println(Udp.localPort());
           return true;
       }
       else
       {
          Serial.print("AutoConfig Waiting......");
          Serial.println(WiFi.status());
          delay(1000);
       }
    }
    delay(1000);
    Serial.println("AutoConfig Faild!" );
    return false;
}

void LianWang()
{
    if (!autoConfig())    //判断网络是否连接成功
    {
       smartConfig();
    }

}
//**************************************************************************************************
void EPD_writeCommand(uint8_t c)
{
  SPI.beginTransaction(spi_settings);
  if (dc >= 0) digitalWrite(dc, LOW);  //dc
  if (cs >= 0) digitalWrite(cs, LOW);  //cs
  SPI.transfer(c);
  if (dc >= 0) digitalWrite(dc, HIGH);   //dc
  if (cs >= 0) digitalWrite(cs, HIGH);   //cs
  SPI.endTransaction();
}

void EPD_writeData(uint8_t d)
{
  SPI.beginTransaction(spi_settings);
  if (cs >= 0) digitalWrite(cs, LOW); //cs
  SPI.transfer(d);
  if (cs >= 0) digitalWrite(cs, HIGH); //cs
  SPI.endTransaction();
}

void xiaobian() //消除黑边（四周的边跟随屏幕刷新，仅全局刷新有效）
{
  EPD_writeCommand(0x3c);  // 边界波形控制寄存器
  EPD_writeData(0x33);     // 向里面写入数据

  //EPD_writeCommand(0x2c); // VCOM setting
  //EPD_writeData(0xA1);    // * different   FPC丝印A1 库默认A8
}


void display_partialLine(uint8_t line, String zf) ////发送局部刷新的显示信息到屏幕,带居中
{
  /*
    display_partialLine()
    发送局部刷新的显示信息到屏幕,带居中

    line        行数（0-7）
    zf          字符内容
    lineRefresh 整行刷新 1-是 0-仅刷新字符长度的区域
  */
  //u8g2Fonts.setFont(chinese_gb2312);
  const char *character = zf.c_str();                            //String转换char
  uint16_t zf_width = u8g2Fonts.getUTF8Width(character);         //获取字符的像素长度
  uint16_t x = (display.width() / 2) - (zf_width / 2);           //计算字符居中的X坐标（屏幕宽度/2-字符宽度/2）
  display.setPartialWindow(0, line * 16, display.width(), 16);   //整行刷新
  display.firstPage();
  do
  {
    u8g2Fonts.setCursor(x, line * 16 + 13);
    u8g2Fonts.print(character);
  }
  while (display.nextPage());
  //display.powerOff(); //关闭屏幕电源
}
/*
void FixedRefresh() //定次刷新,每5次刷黑白一次，每15次正式全局刷黑白
{
  //每6次快速全屏刷新一次，第30次完全全屏刷新
  display.init(0, 0, 10, 0);   //串口使能 初始化完全刷新使能 复位时间 ret上拉使能
  if (pageUpdataCount == 30)  //第30次完全刷新
  {
    display.init(0, 1, 10, 0);
    pageUpdataCount = 0;
  }
  else if (pageUpdataCount % 6 == 0)  //每6次黑白刷新一次
  {
    display.fillScreen(GxEPD_BLACK);  // 填充屏幕
    display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲
    display.fillScreen(GxEPD_WHITE);
    display.display(1);
  }
  else
  {
    display.fillScreen(GxEPD_WHITE);
    if (PageChange != 0 && eepUserSet.fastFlip == 0) display.display(1);//快速翻页关闭
  }
  pageUpdataCount++;
}
*/
void Display_Time()
{
  //0-正常 1-首次获取网络时间失败 2-每10分钟的wifi连接超时 3-每10分钟的获取网络时间失败

  if (Data.RTC_ntpTimeError == 1) 
  {
    display_partialLine(6, "NTP服务器连接失败");
    // display_bitmap_sleep(" ");   //显示休眠图标，并在底下显示原因
    // esp_sleep(0);   //esp休眠
  }
  // if(RTC_hour != RTC_hour||RTC_minute != RTC_minute)
  // {
    //每xx次局刷全刷一次
    if (Data.RTC_jsjs == 0) display.init(0, 1, 10, 0);
    else               display.init(0, 0, 10, 0);
    Data.RTC_jsjs++; //局刷计数
    // ESP.rtcUserMemoryWrite(RTCdz_jsjs, &RTC_jsjs, sizeof(RTC_jsjs));
    display.setPartialWindow(0, 0, display.width(), display.height()); //设置局部刷新窗口
    display.firstPage();
    do
    {
      //************************ 时间显示 ************************
        u8g2Fonts.setFont(u8g2_font_logisoso78_tn);
        //拼装时间 小时和分,不够两位数需要补0
        String hour, minute, assembleTime;
        if (Data.RTC_hour < 10)   hour = "0" + String(Data.RTC_hour);
        else                 hour = String(Data.RTC_hour);
        if (Data.RTC_minute < 10) minute = "0" + String(Data.RTC_minute);
        else                 minute = String(Data.RTC_minute);
        assembleTime = hour + ":" + minute;

        int8_t sz_x = 0; //显示位置X轴的偏移量
        if (Data.RTC_hour >= 10 && Data.RTC_hour <= 19) sz_x = -3; //10-19点
        else sz_x = 2;
        //显示时间
        u8g2Fonts.setCursor(sz_x, 95);
        u8g2Fonts.print(assembleTime);
        //画一条垂直线
        // display.drawLine(230, 7, 230, 103, 0);
    } while (display.nextPage());
  // }
}

void Display_TianQi()
{
   //u8g2Fonts.setFontMode(1);
  display.setFullWindow(); //设置全屏刷新 height
  display.firstPage();
  do
  {
    //display.setPartialWindow(0, 0, display.width(), display.height()); //设置局部刷新窗口

    //display.fillScreen(heise);  // 填充屏幕
    //display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲
    //display.fillScreen(baise);  // 填充屏幕
    //display.display(1);         // 显示缓冲内容到屏幕，用于全屏缓冲

    /* u8g2Fonts.setCursor(275, 16); //实时时间-小时
      u8g2Fonts.print(RTC_hour);
      u8g2Fonts.setCursor(275, 32); //实时时间-小时
      u8g2Fonts.print(RTC_night_count);*/

    //****** 显示实况温度和天气图标 ******
    //提取最后更新时间的 仅提取 小时:分钟
    String minutes_hours;
    for (uint8_t i = 11; i < 16; i++) minutes_hours += actual.last_update[i];

    const char* minutes_hours_c = minutes_hours.c_str();                         //String转换char
    uint16_t minutes_hours_length = u8g2Fonts.getUTF8Width(minutes_hours_c);     //获取最后更新时间的长度
    uint16_t city_length = u8g2Fonts.getUTF8Width(actual.city);                  //获取城市的字符长度
    uint16_t weather_name_length = u8g2Fonts.getUTF8Width(actual.weather_name);  //获取天气现象的字符长度
    uint16_t uvi_x = 284 - (strlen(life_index.uvi) * 26 / 6 + 3);                //UVI字符的位置

    //计算这三个数值谁最大
    int num[3] = {minutes_hours_length, city_length, weather_name_length};
    int len = sizeof(num) / sizeof(*num);
    int sy_length = uvi_x + (get_max_num(num, len) + main_x1); //剩余长度

    u8g2Fonts.setFont(u8g2_font_fub42_tn); //实况温度用字体

    int temp_x_length = u8g2Fonts.getUTF8Width(actual.temp) + 20 + 58;
    temp_x = (sy_length / 2) - (temp_x_length / 2);
    u8g2Fonts.setCursor(temp_x, temp_y0);  //显示实况温度
    u8g2Fonts.print(actual.temp);

    //画圆圈
    circle_x = temp_x + u8g2Fonts.getUTF8Width(actual.temp) + 8; //计算圈圈的位置
    display.drawCircle(circle_x, temp_y0 - 34, 3, 0);
    display.drawCircle(circle_x, temp_y0 - 34, 4, 0);
    //显示天气图标
    // display_tbpd();
    
    //****** 显示小图标和详情信息 ******
    u8g2Fonts.setFont(chinese_city_gb2312);
    //最后更新时间
    // display.drawInvertedBitmap(main_x0, main_y0 - 12, Bitmap_gengxing, 13, 13, heise); //画最后更新时间小图标
    u8g2Fonts.setCursor(main_x1, main_y0);
    u8g2Fonts.print(minutes_hours);
    //城市名
    // display.drawInvertedBitmap(main_x0, main_y1 - 12, Bitmap_weizhi, 13, 13, heise); //画位置小图标
    u8g2Fonts.setCursor(main_x1, main_y1);
    u8g2Fonts.print(actual.city);
    //天气实况状态
    // display.drawInvertedBitmap(main_x0, main_y2 - 12, Bitmap_zhuangtai, 13, 13, heise); //画天气状态小图标
    u8g2Fonts.setCursor(main_x1, main_y2);
    u8g2Fonts.print(actual.weather_name);
    //紫外线指数
    u8g2Fonts.setFont(u8g2_font_u8glib_4_tf);
    u8g2Fonts.setCursor(284, main_y0 - 3);
    u8g2Fonts.print("UVI");
    u8g2Fonts.setFont(chinese_city_gb2312);
    u8g2Fonts.setCursor(uvi_x, main_y0);
    u8g2Fonts.print(life_index.uvi);
    //Serial.print("life_index.uvi长度："); Serial.println(strlen(life_index.uvi));
    //Serial.print("life_index.uvi："); Serial.println(life_index.uvi);
    //湿度
    // display.drawInvertedBitmap(main_x2, main_y1 - 12, Bitmap_humidity, 13, 13, heise);
    u8g2Fonts.setCursor(main_x2 - (strlen(future.date0_humidity) * 6 + 3), main_y1);
    u8g2Fonts.print(future.date0_humidity);
    //风力等级
    // display.drawInvertedBitmap(main_x2, main_y2 - 12, Bitmap_fx, 13, 13, heise);
    u8g2Fonts.setCursor(main_x2 - (strlen(future.date0_wind_scale) * 6 + 3), main_y2);
    u8g2Fonts.print(future.date0_wind_scale);

    
    // //************ 显示未来天气 ************
    // //拼装月日字符串 格式02-02
    // String day0, day1, day2;
    // for (uint8_t i = 5; i < 10; i++)
    // {
    //   day0 += future.date0[i];
    //   day1 += future.date1[i];
    //   day2 += future.date2[i];
    // }

    // //拼装星期几
    // //提取年月日并转换成int
    // String nian0, nian1, nian2, yue0, yue1, yue2, ri0, ri1, ri2;
    // int nian0_i, nian1_i, nian2_i, yue0_i, yue1_i, yue2_i, ri0_i, ri1_i, ri2_i;
    // for (uint8_t i = 0; i <= 9; i++)
    // {
    //   if (i <= 3)
    //   {
    //     nian0 += future.date0[i];
    //     nian1 += future.date1[i];
    //     nian2 += future.date2[i];
    //   }
    //   else if (i == 5 || i == 6)
    //   {
    //     yue0 += future.date0[i];
    //     yue1 += future.date1[i];
    //     yue2 += future.date2[i];
    //   }
    //   else if (i == 8 || i == 9)
    //   {
    //     ri0 += future.date0[i];
    //     ri1 += future.date1[i];
    //     ri2 += future.date2[i];
    //   }
    // }
    // nian0_i = atoi(nian0.c_str()); yue0_i = atoi(yue0.c_str()); ri0_i = atoi(ri0.c_str());
    // nian1_i = atoi(nian1.c_str()); yue1_i = atoi(yue1.c_str()); ri1_i = atoi(ri1.c_str());
    // nian2_i = atoi(nian2.c_str()); yue2_i = atoi(yue2.c_str()); ri2_i = atoi(ri2.c_str());

    // //Serial.print("年:"); Serial.print(nian0_i); Serial.print(" "); Serial.print(nian1_i); Serial.print(" "); Serial.println(nian2_i);
    // //Serial.print("月:"); Serial.print(yue0_i); Serial.print(" "); Serial.print(yue1_i); Serial.print(" "); Serial.println(yue2_i);
    // //Serial.print("日:"); Serial.print(ri0_i); Serial.print(" "); Serial.print(ri1_i); Serial.print(" "); Serial.println(ri2_i);
    // //Serial.println(week_calculate(nian, yue, ri));

    // //拼装白天和晚上的天气现象
    // String text_day0, text_night0, dn0_s;
    // String text_day1, text_night1, dn1_s;
    // String text_day2, text_night2, dn2_s;
    // /*strcmp(const char s1,const char s2)
    //   当 str1 < str2 时，返回为负数(-1)；
    //   当 str1 == str2 时，返回值= 0；
    //   当 str1 > str2 时，返回正数(1)。*/

    // if (strcmp(future.date0_text_day, future.date0_text_night) != 0) //今天
    // {
    //   text_day0 = future.date0_text_day;
    //   text_night0 = future.date0_text_night;
    //   dn0_s = text_day0 + "转" + text_night0;
    // }
    // else dn0_s = future.date0_text_night;

    // if (strcmp(future.date1_text_day, future.date1_text_night) != 0) //明天
    // {
    //   text_day1 = future.date1_text_day;
    //   text_night1 = future.date1_text_night;
    //   dn1_s = text_day1 + "转" + text_night1;
    // }
    // else dn1_s = future.date1_text_night;

    // if (strcmp(future.date2_text_day, future.date2_text_night) != 0) //后天
    // {
    //   text_day2 = future.date2_text_day;
    //   text_night2 = future.date2_text_night;
    //   dn2_s = text_day2 + "转" + text_night2;
    // }
    // else dn2_s = future.date2_text_night;

    // //拼装高低温
    // String  high0, high1, high2, low0, low1, low2, hl0_s, hl1_s, hl2_s;
    // high0 = future.date0_high; high1 = future.date1_high; high2 = future.date2_high;
    // low0 = future.date0_low; low1 = future.date1_low; low2 = future.date2_low;
    // hl0_s = high0 + "/" + low0;
    // hl1_s = high1 + "/" + low1;
    // hl2_s = high2 + "/" + low2;

    // //拼装未来天气详情并显示
    // String wltqxq0 = "今 " + day0 + " " + week_calculate(nian0_i, yue0_i, ri0_i) + " " + dn0_s + " " + hl0_s;
    // String wltqxq1 = "明 " + day1  + " " + week_calculate(nian1_i, yue1_i, ri1_i) + " " + dn1_s + " " + hl1_s;
    // String wltqxq2 = "后 " + day2  + " " + week_calculate(nian2_i, yue2_i, ri2_i)  + " " + dn2_s + " " + hl2_s;
    // //计数长度
    // uint16_t wltqxq0_length = u8g2Fonts.getUTF8Width(wltqxq0.c_str());
    // uint16_t wltqxq1_length = u8g2Fonts.getUTF8Width(wltqxq1.c_str());
    // uint16_t wltqxq2_length = u8g2Fonts.getUTF8Width(wltqxq2.c_str());
    // //自动居中的X坐标
    // int wltqxq0_x, wltqxq1_x, wltqxq2_x;
    // wltqxq0_x = (display.width() / 2) - (wltqxq0_length / 2);
    // wltqxq1_x = (display.width() / 2) - (wltqxq1_length / 2);
    // wltqxq2_x = (display.width() / 2) - (wltqxq2_length / 2);
    // //拼装数据 分成2段（今 06-21）&（周一 阴转阵雨）
    // String data0 = "今 " + day0; //今 06-21
    // String data1 = week_calculate(nian0_i, yue0_i, ri0_i) + " " + dn0_s; //周一 阴转阵雨
    // String data2 = "明 " + day1; //明 06-22
    // String data3 = week_calculate(nian1_i, yue1_i, ri1_i) + " " + dn1_s; //周二 阵雨转大雨
    // String data4 = "后 " + day2; //后 06-23
    // String data5 = week_calculate(nian2_i, yue2_i, ri2_i) + " " + dn2_s; //周三 大雨转中雨

    // uint16_t data0_length = u8g2Fonts.getUTF8Width(data0.c_str());
    // uint16_t data1_length = u8g2Fonts.getUTF8Width(data1.c_str());
    // uint16_t data2_length = u8g2Fonts.getUTF8Width(data2.c_str());
    // uint16_t data3_length = u8g2Fonts.getUTF8Width(data3.c_str());
    // uint16_t data4_length = u8g2Fonts.getUTF8Width(data4.c_str());
    // uint16_t data5_length = u8g2Fonts.getUTF8Width(data5.c_str());
    // //对比三个数谁大
    // int data_x0_num[3] = {data0_length, data2_length, data4_length};
    // int data_x0_len = sizeof(data_x0_num) / sizeof(*data_x0_num);
    // int data_x0_max = get_max_num(data_x0_num, data_x0_len);
    // //对比三个数谁大
    // int data_x1_num[3] = {data1_length, data3_length, data5_length};
    // int data_x1_len = sizeof(data_x1_num) / sizeof(*data_x1_num);
    // int data_x1_max = get_max_num(data_x1_num, data_x1_len);
    // //对比三个数谁小
    // int wltqxq_num[3] = {wltqxq0_x, wltqxq1_x, wltqxq2_x};
    // int wltqxq_len = sizeof(wltqxq_num) / sizeof(*wltqxq_num);
    // int wltqxq_x_min = get_min_num(wltqxq_num, wltqxq_len) + 2;
    // //数据之间的间隔 5个像素
    // uint8_t jianGe_x = 5;

    // u8g2Fonts.setCursor(wltqxq_x_min, day_y0);
    // u8g2Fonts.print(data0);
    // u8g2Fonts.setCursor(wltqxq_x_min + data_x0_max + jianGe_x, day_y0);
    // u8g2Fonts.print(data1);
    // u8g2Fonts.setCursor(wltqxq_x_min + data_x0_max + jianGe_x + data_x1_max + jianGe_x, day_y0);
    // u8g2Fonts.print(hl0_s);

    // u8g2Fonts.setCursor(wltqxq_x_min, day_y1);
    // u8g2Fonts.print(data2);
    // u8g2Fonts.setCursor(wltqxq_x_min + data_x0_max + jianGe_x, day_y1);
    // u8g2Fonts.print(data3);
    // u8g2Fonts.setCursor(wltqxq_x_min + data_x0_max + jianGe_x + data_x1_max + jianGe_x, day_y1);
    // u8g2Fonts.print(hl1_s);

    // u8g2Fonts.setCursor(wltqxq_x_min, day_y2);
    // u8g2Fonts.print(data4);
    // u8g2Fonts.setCursor(wltqxq_x_min + data_x0_max + jianGe_x, day_y2);
    // u8g2Fonts.print(data5);
    // u8g2Fonts.setCursor(wltqxq_x_min + data_x0_max + jianGe_x + data_x1_max + jianGe_x, day_y2);
    // u8g2Fonts.print(hl2_s);
  }
  while (display.nextPage());
}
void Display_WenShiDu()
{
    if (Data.RTC_jsjs == 0) display.init(0, 1, 10, 0);
    else               display.init(0, 0, 10, 0);
    Data.RTC_jsjs++; //局刷计数
    // ESP.rtcUserMemoryWrite(RTCdz_jsjs, &RTC_jsjs, sizeof(RTC_jsjs));
    display.setPartialWindow(0, 0, display.width(), display.height()); //设置局部刷新窗口
    display.firstPage();
    do
    {
      //************************ 温湿度显示 ************************
        u8g2Fonts.setFont(chinese_city_gb2312);
        //拼装时间 小时和分,不够两位数需要补0
        String Humidity, Temperature, Heat_index,dis_H,dis_T,dis_index;
        Humidity = String(Data.DHT11_Humidity);
        Temperature = String(Data.DHT11_Temperature);
        Heat_index = String(Data.Heat_index);
        dis_H = "湿度:" + Humidity + "%"; 
        dis_T = "温度:" + Temperature + "°C";
        dis_index ="热指数:" + Heat_index + "°C";
        //显示温度
        u8g2Fonts.setCursor(10, 20);
        u8g2Fonts.print(dis_T);

        u8g2Fonts.setCursor(10, 40);
        u8g2Fonts.print(dis_H);

        u8g2Fonts.setCursor(10, 60);
        u8g2Fonts.print(dis_index);

        //画一条垂直线
        // display.drawLine(230, 7, 230, 103, 0);
    } while (display.nextPage());
  
}
//****** 冒泡算法从小到大排序 ******
int get_max_num(int arr[], int len)
{
  int i, j;

  for (i = 0; i < len - 1; i++)
  {
    for (j = 0; j < len - 1 - i; j++)
    {
      if (arr[j] > arr[j + 1])
      {
        swap(&arr[j], &arr[j + 1]);
      }
    }
  }
  return arr[len - 1]; //获取最大值
}

int get_min_num(int arr[], int len)
{
  int i, j;

  for (i = 0; i < len - 1; i++)
  {
    for (j = 0; j < len - 1 - i; j++)
    {
      if (arr[j] > arr[j + 1])
      {
        swap(&arr[j], &arr[j + 1]);
      }
    }
  }
  return arr[0]; //获取最小值
}

void swap(int *t1, int *t2)
{
  int temp;
  temp = *t1;
  *t1 = *t2;
  *t2 = temp;
}
