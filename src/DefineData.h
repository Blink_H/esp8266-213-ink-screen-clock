#ifndef _DefineData_h
#define _DefineData_h

#include <Arduino.h>


// //RTC临时数据
// extern uint32_t RTC_minute = 100;       // RTC数据-分钟
// extern uint32_t RTC_hour = 100;         // RTC数据-小时
// // uint32_t RTC_seconds = 100;      // RTC数据-秒
// // uint32_t RTC_cq_cuont = 0;       // RTC数据-重启计数
// // uint32_t RTC_clockTime2 = 0;     // RTC数据-上次的时间
// // uint32_t RTC_wifi_ljcs = 0;      // RTC数据-wifi连接超时计数
// extern uint32_t RTC_jsjs = 0;           // RTC数据-局刷计数
// extern uint32_t RTC_ntpTimeError = 0;   // 获取时间超时 0-无 1-超时 2-wifi连接超时
// // uint32_t RTC_clock_code = 0;     // 时钟错误代码

struct Define_Data
{
  uint32_t RTC_hour;
  uint32_t RTC_minute;
  uint32_t RTC_jsjs;
  uint32_t RTC_ntpTimeError;
  uint32_t RTC_clock_code;
  float DHT11_Temperature;  //温度
  float DHT11_Humidity;     //湿度
  float Heat_index;        //热指数
};
//声明引用外部结构体DefineData里的变量
extern Define_Data Data;
#pragma once  //只编译一次

#endif