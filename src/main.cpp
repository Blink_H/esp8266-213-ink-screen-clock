#include <Arduino.h>
#include "display_demo.h"
#include "GetData.h"
/*
   8266接线方式：
   busy--D2 4
   res--D4 2
   dc--D3 0
   cs--D8 15
   clk--D5 14
   din--D7 13
   gnd--g
   vcc--3v3
*/
void setup()
{
  Serial.begin(115200);
  Serial.println("setup");
  display_init(); 
  xiaobian();
  Serial.println("Start module");
  delay(00);
  LianWang();
  NTP_init();
  DHT11_init();
}

void loop()
{
   // Get_Time();
   // Display_Time();

   // Get_TianQi();                // 获取数据界面
   // Display_TianQi();           // 显示主界面
   delay(5000);
   Get_Dht11data();
   Display_WenShiDu();
}

