#ifndef _Displaydemo_h
#define _Displaydemo_h

#include <Arduino.h>
//公用数据定义
#include "DefineData.h"
// wifi连接函数
#include <ESP8266WiFi.h>

#include <Wire.h>
// U8g2 for Adafruit GFX：显示文字
#include <U8g2_for_Adafruit_GFX.h>
// 基于Adafruit_GFX库驱动各种电子墨水屏
#include <GxEPD2_BW.h>
#include <SPI.h>
// 字体函数
#include "gb2312.c"
// #include "img.c"
#include "ErWeima.c"

#include "JsonWeather.h"
#include "Other.h"


#define RST_PIN 0       // D3(0)

#define dc 0
#define cs 16



void display_init();   // 2.13寸屏幕初始化
void BW_refresh(); //黑白刷新一次
void display_ErWeiMa();    //显示二维码

// // 向给定地址的时间服务器发送NTP请求
// void sendNTPpacket(IPAddress &address);
// time_t getNtpTime();  //回去NNTP时间函数
// void smartConfig();  //智能配网函数
// bool autoConfig();//wifi自动连接函数

// spi发送数据函数
void EPD_writeCommand(uint8_t c);
void EPD_writeData(uint8_t d);

void xiaobian(); //消除黑边（四周的边跟随屏幕刷新，仅全局刷新有效）

// void display_time(); //显示时间函数

void LianWang();  //联网

void NTP_init();  //NPT时钟初始化

void display_partialLine(uint8_t line, String zf);
void FixedRefresh();//定次刷新,每5次刷黑白一次，每15次正式全局刷黑白
void Display_Time();

void Display_TianQi();

void Display_WenShiDu();

int get_max_num(int arr[], int len);
int get_min_num(int arr[], int len);
void swap(int *t1, int *t2);

#endif
